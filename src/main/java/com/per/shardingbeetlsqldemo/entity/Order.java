package com.per.shardingbeetlsqldemo.entity;

import lombok.*;
import org.beetl.sql.core.annotatoin.AutoID;

import java.io.Serializable;

/**
 * @author: Cheng
 * @date: 2019/12/9 21:57
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Order implements Serializable {
    private static final long serialVersionUID = -21353099551913779L;

    @AutoID
    private Integer orderId;

    private Integer userId;

    private String name;
}
