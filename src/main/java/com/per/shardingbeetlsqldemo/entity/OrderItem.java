package com.per.shardingbeetlsqldemo.entity;

import lombok.*;
import org.beetl.sql.core.annotatoin.AutoID;

import java.io.Serializable;

/**
 * @author: Cheng
 * @date: 2019/12/9 21:59
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class OrderItem implements Serializable {
    private static final long serialVersionUID = -21343099551913779L;

    @AutoID
    private Integer orderId;

    private String item;

    private Integer userId;
}
