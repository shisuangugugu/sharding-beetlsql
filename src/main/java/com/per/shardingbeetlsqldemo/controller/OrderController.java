package com.per.shardingbeetlsqldemo.controller;

import com.per.shardingbeetlsqldemo.service.OrderService;
import com.per.shardingbeetlsqldemo.util.ResultUtil;
import com.per.shardingbeetlsqldemo.vo.ResultVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: Cheng
 * @date: 2019/12/9 22:06
 */
@RestController
@RequestMapping("/order")
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
@Slf4j
public class OrderController {

    private final OrderService orderService;

    @PostMapping("/add")
    public ResultVO addOrder(Integer userId, String name) {
        orderService.addOrder(userId, name);
        return ResultUtil.success();
    }

    @GetMapping("/get")
    public ResultVO getOrder(Integer orderId, Integer userId) {
        return ResultUtil.success(orderService.getOrder(orderId, userId));
    }

    @GetMapping("/all")
    public ResultVO getAll() {
        return ResultUtil.success(orderService.getAll());
    }
}
