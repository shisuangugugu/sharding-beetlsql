package com.per.shardingbeetlsqldemo.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.per.shardingbeetlsqldemo.exception.IErrorCode;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResultVO<T> implements Serializable {
    private static final long serialVersionUID = -5223036380579501065L;

    private Integer code;
    private String msg;
    private Integer count;
    private T data;

    public ResultVO() {

    }

    public ResultVO(IErrorCode errorCode) {
        this.code = errorCode.getCode();
        this.msg = errorCode.getMsg();
    }
}
