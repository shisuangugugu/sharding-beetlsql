package com.per.shardingbeetlsqldemo.exception;

public enum ErrorCodeEnums implements IErrorCode {
    /**
     * 正常执行
     */
    SUCCESS(0, "处理成功"),
    /**
     * 异常执行过程
     */
    SERVER_INTERNAL_ERROR(-1, "服务器内部错误"),
    /**
     * 意料之外的异常，或无法归类的异常
     */
    UNKNOWN_ERROR(-100, "未知错误"),
    /**
     * 数据库操作异常
     */
    DB_ERROR(-101, "数据库错误"),
    /**
     * 无权限访问接口异常
     */
    UNAUTHORIZED_URL(401, "未授权访问"),
    /**
     * 对象或接口等不存在异常
     */
    NOT_FOUND(404, "该对象不存在"),
    /**
     * 参数设置异常
     */
    BIND_ARGS_ERROR(600, "参数绑定异常"),
    /**
     * 参数校验失败
     */
    ARGS_VALIDATE_ERROR(600, "参数校验异常")
    ;

    private Integer code;
    private String msg;

    ErrorCodeEnums(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }

    public IErrorCode setMsg(String msg) {
        this.msg = msg;
        return this;
    }
}
