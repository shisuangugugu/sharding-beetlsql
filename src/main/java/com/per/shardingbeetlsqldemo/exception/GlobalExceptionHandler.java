package com.per.shardingbeetlsqldemo.exception;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.per.shardingbeetlsqldemo.util.ResultUtil;
import com.per.shardingbeetlsqldemo.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.beetl.sql.core.BeetlSQLException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResultVO<?> defaultExceptionHandler(HttpServletRequest request, Exception e) {
        IErrorCode errorCode = getErrorCode(e);
        // 将异常信息转换成json样式，更加直观
        String message = e.getMessage();
        Object errMsgObj = message;

        if (JSONUtil.isJson(message)) {
            errMsgObj = JSONUtil.parseObj(message);
        }

        JSONObject errLogInfo = Objects.requireNonNull(JSONUtil.createObj()
                .put("reqUri", request.getRequestURI()))
                .put("errMsg", errMsgObj);
        log.warn(Objects.requireNonNull(errLogInfo).toString(), e);

        return ResultUtil.failure(errorCode);
    }

    private IErrorCode getErrorCode(Exception e) {
        IErrorCode errorCode = ErrorCodeEnums.SERVER_INTERNAL_ERROR;
        if (e instanceof IFrameException) {
            IFrameException platformException = (IFrameException) e;
            // 当用户抛出的异常是调用带有错误码的构造器构造的时候
            if (platformException.getErrorCode() != null) {
                errorCode = platformException.getErrorCode();
            }
        } else if (e instanceof BindException) {
            BindException bindException = (BindException) e;
            // bindException.getFieldError()
            errorCode = ErrorCodeEnums.BIND_ARGS_ERROR.setMsg(Objects.requireNonNull(bindException.getFieldError()).getDefaultMessage());
        } else if (e instanceof BeetlSQLException) {
            errorCode = ErrorCodeEnums.DB_ERROR;
        } else if (e instanceof MissingServletRequestParameterException) {
            MissingServletRequestParameterException exception = (MissingServletRequestParameterException) e;
            errorCode = ErrorCodeEnums.BIND_ARGS_ERROR.setMsg(StrUtil.format("参数{}为空", exception.getParameterName()));
        }

        return errorCode;
    }
}
