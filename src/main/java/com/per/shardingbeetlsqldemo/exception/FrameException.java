package com.per.shardingbeetlsqldemo.exception;

import cn.hutool.core.util.StrUtil;

public class FrameException extends Exception implements IFrameException {
    private static final long serialVersionUID = -8325396926857034510L;
    private IErrorCode errorCode;


    public FrameException() {
        super();
    }

    /**
     * 直接使用IErrorCode构造异常，异常的msg将使用errorCode.getMsg();
     * 如果errorCode为Null，底层则会自己抛出空指针异常
     *
     * @param errorCode 错误码
     */
    public FrameException(IErrorCode errorCode) {
        this(errorCode, "");
    }

    /**
     * 在构造异常时，除了提供错误码之外，还提供额外的描述信息，这些额外的描述信息
     * 不会返回给用户，用户可以看到的只有错误码中的对应信息，这些额外的信息将会被记录到
     * 日志中，方便出问题时的排查
     *
     * @param errorCode 错误码
     * @param message   额外的信息
     */
    public FrameException(IErrorCode errorCode, String message) {
        this(null, errorCode, message);
    }

    public FrameException(Throwable e, IErrorCode errorCode) {
        this(e, errorCode, "");
    }

    public FrameException(Throwable e, IErrorCode errorCode, String message) {
        super(StrUtil.format("{\"code\": {}, \"msg\": \"{}\", \"extMsg\": \"{}\"}",
                errorCode.getCode(), errorCode.getMsg(), message), e);
        this.errorCode = errorCode;
    }

    public FrameException(String message) {
        super(message);
    }

    public FrameException(String message, Throwable cause) {
        super(message, cause);
    }

    public FrameException(Throwable cause) {
        super(cause);
    }

    protected FrameException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    @Override
    public IErrorCode getErrorCode() {
        return errorCode;
    }
}
