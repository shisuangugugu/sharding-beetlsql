package com.per.shardingbeetlsqldemo.exception;

public interface IErrorCode {
    Integer getCode();
    String getMsg();
}
