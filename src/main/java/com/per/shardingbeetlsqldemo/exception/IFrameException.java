package com.per.shardingbeetlsqldemo.exception;

public interface IFrameException {
    IErrorCode getErrorCode();
}
