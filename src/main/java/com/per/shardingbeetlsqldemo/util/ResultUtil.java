package com.per.shardingbeetlsqldemo.util;

import cn.hutool.core.util.StrUtil;
import com.per.shardingbeetlsqldemo.exception.ErrorCodeEnums;
import com.per.shardingbeetlsqldemo.exception.IErrorCode;
import com.per.shardingbeetlsqldemo.vo.ResultVO;

public class ResultUtil {

    public static <T> ResultVO<T> success() {
        return success(null);
    }

    public static <T> ResultVO<T> success(T data) {
        return success(null, null, data);
    }

    public static <T> ResultVO<T> success(String msg) {
        return success(null, msg, null);
    }

    public static <T> ResultVO<T> success(Integer count, T data) {
        return success(count, null, data);
    }

    public static <T> ResultVO<T> success(Integer count, String msg) {
        return success(count, msg, null);
    }

    /**
     * 返回成功的结果
     * @param count 结果条数
     * @param data  返回的结果
     */
    public static <T> ResultVO<T> success(Integer count, String msg, T data) {
        ResultVO<T> resultVO = new ResultVO<>(ErrorCodeEnums.SUCCESS);
        resultVO.setMsg(msg);
        resultVO.setCount(count);
        resultVO.setData(data);
        return resultVO;
    }

    /**
     * 返回失败的结果，调用该方法时，由于是无参的，因此返回默认的SERVER_INTERNAL_ERROR(服务器内部错误)
     */
    public static ResultVO failure() {
        return failure(ErrorCodeEnums.SERVER_INTERNAL_ERROR);
    }

    /**
     * 错误信息是一个错误码，返回的结果为错误码定义的内容
     *
     * @param errorCode 错误码
     */
    public static ResultVO failure(IErrorCode errorCode) {
        return failure(errorCode, null);
    }

    /**
     * 当调用该方法时，即表示错误码中的msg定义不能满足需求，需要自定义msg，因此，返回给用户
     * 的结果中的msg为自定义的msg，替换掉错误码的默认值
     *
     * @param errorCode 错误码
     * @param msg       自定义信息
     */
    public static ResultVO failure(IErrorCode errorCode, String msg) {
        ResultVO resultVO = new ResultVO(errorCode);
        if (StrUtil.isNotBlank(msg)) {
            resultVO.setMsg(msg);
        }
        return resultVO;
    }
}
