package com.per.shardingbeetlsqldemo.dao;

import com.per.shardingbeetlsqldemo.entity.OrderItem;
import org.beetl.sql.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * @author: Cheng
 * @date: 2019/12/9 22:03
 */
@Repository
public interface OrderItemDao extends BaseMapper<OrderItem> {
}
