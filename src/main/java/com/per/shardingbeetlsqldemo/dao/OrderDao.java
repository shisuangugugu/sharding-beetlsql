package com.per.shardingbeetlsqldemo.dao;

import com.per.shardingbeetlsqldemo.entity.Order;
import org.beetl.sql.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * @author: Cheng
 * @date: 2019/12/9 22:02
 */
@Repository
public interface OrderDao extends BaseMapper<Order> {
}
