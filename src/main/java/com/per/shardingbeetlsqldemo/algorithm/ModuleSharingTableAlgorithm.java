package com.per.shardingbeetlsqldemo.algorithm;

import io.shardingsphere.api.algorithm.sharding.PreciseShardingValue;
import io.shardingsphere.api.algorithm.sharding.standard.PreciseShardingAlgorithm;

import java.util.Collection;

/**
 * @author: Cheng
 * @date: 2019/12/10 10:58
 */
public class ModuleSharingTableAlgorithm implements PreciseShardingAlgorithm<Integer> {

    public ModuleSharingTableAlgorithm() {

    }

    @Override
    public String doSharding(Collection<String> collection, PreciseShardingValue<Integer> preciseShardingValue) {
        System.out.println("进入分片算法");
        for (String a : collection) {
            System.out.println(a);
        }
        System.out.println("column = " + preciseShardingValue.getColumnName());
        System.out.println("table = " + preciseShardingValue.getLogicTableName());
        System.out.println("value = " + preciseShardingValue.getValue());
        for (String each : collection) {
            return each + preciseShardingValue.getValue();
        }
        throw new UnsupportedOperationException();
    }
}
