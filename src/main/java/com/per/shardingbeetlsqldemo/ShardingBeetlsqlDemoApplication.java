package com.per.shardingbeetlsqldemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShardingBeetlsqlDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShardingBeetlsqlDemoApplication.class, args);
    }

}
